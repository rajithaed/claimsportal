namespace ClaimsPortal.API.AppConfig
{
    public interface IApplicationConfiguration
    {
        string AzureSqlConnectionString { get;}   
         
    }
}