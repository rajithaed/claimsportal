﻿using Microsoft.Framework.Configuration;

namespace ClaimsPortal.API.AppConfig
{
    public class ApplicationConfiguration : IApplicationConfiguration
    {
        private readonly IConfiguration _configuration;

        public ApplicationConfiguration(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string AzureSqlConnectionString => _configuration["connectionStrings:AzureSqlConnectionString"];
    }
}