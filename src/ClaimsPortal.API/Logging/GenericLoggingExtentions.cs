﻿namespace ClaimsPortal.API.Logging
{
    public static class GenericLoggingExtentions
    {
        public static ILoggerAdapter Log<T>(this T thing)
        {
            var log = LogManager.GetLogger<T>();
            return log;
        }
    }
}