﻿using System;
using System.Net;
using ClaimsPortal.API.Helpers;
using log4net;

namespace ClaimsPortal.API.Logging
{
    public class LoggerAdapter : ILoggerAdapter
    {
        private readonly ILog _log;

        internal LoggerAdapter(ILog log)
        {
            _log = log;
            AssignAdditionalParametersForLogging();
        }

        private string Browser { get; set; }
        private string RawUrl { get; set; }
        private string UsersIp { get; set; }
        private string HostName { get; set; }

        public void Debug(object message)
        {
            _log.Debug(message);
        }

        public void Debug(object message, Exception exception)
        {
            _log.Debug(message, exception);
        }

        public void Info(object message)
        {
            _log.Info(message);
        }

        public void Info(object message, Exception exception)
        {
            _log.Info(message, exception);
        }

        public void Warn(object message)
        {
            _log.Warn(message);
        }

        public void Warn(object message, Exception exception)
        {
            _log.Warn(message, exception);
        }

        public void Error(object message)
        {
            _log.Error(message);
        }

        public void Error(object message, Exception exception)
        {
            _log.Error(message, exception);
        }

        public void Fatal(object message)
        {
            _log.Fatal(message);
        }

        public void Fatal(object message, Exception exception)
        {
            _log.Fatal(message, exception);
        }

        private void AssignAdditionalParametersForLogging()
        {
            try
            {
                var httpContext = HttpContextExtensionsHelper.GetHttpContext();

                HostName = Dns.GetHostName();
                UsersIp = httpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();


                var url = httpContext.Request.Host.Value;
                var path = httpContext.Request.Path.Value;
                RawUrl = url + path;

                Browser = httpContext.Request.Headers.GetValues("User-Agent")[0];

                LogicalThreadContext.Properties["host_name"] = HostName;
                LogicalThreadContext.Properties["user_ip"] = UsersIp;
                LogicalThreadContext.Properties["raw_url"] = RawUrl;
                LogicalThreadContext.Properties["browser"] = Browser;
            }
            catch (Exception ex)
            {
                Error("Error occured when getting values from HttpContext to log4net parameters.", ex);
            }
        }
    }
}