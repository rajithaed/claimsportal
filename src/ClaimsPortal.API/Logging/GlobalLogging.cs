using System;

namespace ClaimsPortal.API.Logging
{
    public class GlobalLogging
    {
        public static void LogUnHandledException(object sender, UnhandledExceptionEventArgs e)
        {
            LogManager.GetLogger<GlobalLogging>()
                .Error($"{(e.IsTerminating ? string.Empty : "Non-")}Fatal Unhandled Exception",
                    e.ExceptionObject as Exception);

            throw (Exception) e.ExceptionObject;
        }
    }
}