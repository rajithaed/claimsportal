﻿using System;
using log4net.Config;

namespace ClaimsPortal.API.Logging
{
    public class LogManager : ILogManager
    {
        private static readonly ILogManager _logManager;

        static LogManager()
        {
            XmlConfigurator.Configure();
            _logManager = new LogManager();
        }

        public ILoggerAdapter GetLogger(Type type)
        {
            var logger = log4net.LogManager.GetLogger(type);
            return new LoggerAdapter(logger);
        }

        public static ILoggerAdapter GetLogger<T>()
        {
            return _logManager.GetLogger(typeof (T));
        }
    }


    public interface ILogManager
    {
        ILoggerAdapter GetLogger(Type type);
    }

    public interface ILoggerAdapter
    {
        void Debug(object message);
        void Debug(object message, Exception exception);
        void Info(object message);
        void Info(object message, Exception exception);
        void Warn(object message);
        void Warn(object message, Exception exception);
        void Error(object message);
        void Error(object message, Exception exception);
        void Fatal(object message);
        void Fatal(object message, Exception exception);
    }
}