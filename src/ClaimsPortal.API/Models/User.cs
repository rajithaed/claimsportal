﻿namespace ClaimsPortal.API.Models
{
    public class User
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public AccessLevel AccessLevel { get; set; }
    }

    public class UserDetail
    {
        public int Id { get;}
        public string Name { get; }

       public UserDetail(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }

    public enum AccessLevel
    {
        Admin,
        SuperUser,
        Manager,
        Agent,
        ExternalUser
    }
}