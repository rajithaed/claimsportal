﻿using System.Collections.Generic;
using ClaimsPortal.API.AppConfig;
using ClaimsPortal.API.Models;
using ClaimsPortal.API.Repositories;
using Microsoft.AspNet.Mvc;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ClaimsPortal.API.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly IApplicationConfiguration _appConfig;
        private readonly IUserRepository _userRepository;

        public UserController(IApplicationConfiguration appConfig)
        {
            _appConfig = appConfig;
            _userRepository = new UserRepository(_appConfig);
        }

        [HttpGet]
        public IActionResult Get()
        {
            var result = _userRepository.GetAllUsers();
            return Json(result);
        }
        
        [HttpGet("details/{id}")]
        public IActionResult Get(int id)
        {
            var result = _userRepository.GetUserDetails(id);
            return Json(result);
        }

      
        [HttpPost]
        public bool Post([FromBody] UserDetail value)
        {
            return _userRepository.UpdateUser(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}