﻿using System.Collections.Generic;
using ClaimsPortal.API.Models;

namespace ClaimsPortal.API.Repositories
{
    public interface IUserRepository
    {
        IEnumerable<User> GetAllUsers();
        UserDetail GetUserDetails(int userId);
        bool UpdateUser(UserDetail updatedUser);
    }
}