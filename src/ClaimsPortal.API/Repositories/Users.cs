using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using ClaimsPortal.API.AppConfig;
using ClaimsPortal.API.Logging;
using ClaimsPortal.API.Models;

namespace ClaimsPortal.API.Repositories
{
    public class Users
    {
        private readonly IApplicationConfiguration _appConfig;
        private readonly string _sqlConnectionString;

        public Users(IApplicationConfiguration appConfig)
        {
            _appConfig = appConfig;
            _sqlConnectionString = _appConfig.AzureSqlConnectionString;
        }

        public List<User> GetAllUsersFromDb()
        {
            var GetAllUsersQuery = @"
select * from Users with(nolock) order by ID asc 
";
            try
            {
                using (var connection = new SqlConnection(_sqlConnectionString))
                {
                    using (var command = new SqlCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = GetAllUsersQuery;
                        command.Connection = connection;

                        if (connection.State != ConnectionState.Open)
                        {
                            connection.Open();
                        }
                        var ds = new DataSet();
                        using (var adapter = new SqlDataAdapter(command))
                        {
                            adapter.Fill(ds);
                        }

                        return MapToUsersList(ds);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log().Error(ex);
                throw;
            }
        }

        private List<User> MapToUsersList(DataSet ds)
        {
            var rows = ds.Tables[0].Rows;
            return rows
                .OfType<DataRow>()
                .Select(dr => new User {Id = (int) dr[0], UserName = (string) dr[1], Password = (string) dr[2]})
                .ToList();
        }


        public UserDetail GetUserDetailsFromDb(int userId)
        {
            var GetUserDetailsQuery = @"
select * from Users with(nolock) where ID=@UserId
";
            try
            {
                using (var connection = new SqlConnection(_sqlConnectionString))
                {
                    using (var command = new SqlCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = GetUserDetailsQuery;
                        command.Connection = connection;

                        command.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int));
                        command.Parameters["@UserId"].Value = userId;

                        if (connection.State != ConnectionState.Open)
                        {
                            connection.Open();
                        }
                        var ds = new DataSet();
                        using (var adapter = new SqlDataAdapter(command))
                        {
                            adapter.Fill(ds);
                        }

                        return MapToUser(ds);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log().Error(ex);
                throw;
            }
        }

        private UserDetail MapToUser(DataSet ds)
        {
            if (ds.Tables.Count == 0) return null;
            var t = ds.Tables[0];
            if (t.Rows.Count == 0) return null;

            var row = t.Rows[0];
            return new UserDetail(
                (int) row.ItemArray[0],
                (string) row.ItemArray[1]
                );
        }

        public bool UpdateUserDetails(UserDetail updatedUser)
        {
            var UpdateUserDetailsQuery = @"
UPDATE Users 
SET user_name = @UserName
WHERE ID = @UserId
";

            using (var connection = new SqlConnection(_sqlConnectionString))
            {
                var command = new SqlCommand(UpdateUserDetailsQuery, connection);
                
                command.Parameters.Add("@UserId", SqlDbType.Int);
                command.Parameters["@UserId"].Value = updatedUser.Id;

                command.Parameters.Add("@UserName", SqlDbType.Char);
                command.Parameters["@UserName"].Value = updatedUser.Name;

                try
                {
                    connection.Open();
                    var rowsAffected = command.ExecuteNonQuery();
                    return true;
                }
                catch (Exception ex)
                {
                    this.Log().Error(ex);
                    return false;
                }
            }
        }
    }
}