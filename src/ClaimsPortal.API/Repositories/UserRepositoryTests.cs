﻿using System.Collections.Generic;
using System.Linq;
using ClaimsPortal.API.AppConfig;
using ClaimsPortal.API.Models;
using NSubstitute;
using NSubstitute.Core;
using Xunit;

namespace ClaimsPortal.API.Repositories
{
    public class UserRepositoryTests
    {
        private IApplicationConfiguration _appConfig;
        private List<User> _listOfUsers;
        private IUserRepository _userRepository;
        private UserDetail _user;

        public UserRepositoryTests()
        {
            TestSetUp();
        }

        private void TestSetUp()
        {
            _listOfUsers = new List<User>
            {
                new User {Id = 1, UserName = "Raj", Password = "123", AccessLevel = AccessLevel.Admin},
                new User {Id = 1, UserName = "Joe", Password = "456", AccessLevel = AccessLevel.SuperUser},
                new User {Id = 1, UserName = "Alex", Password = "789", AccessLevel = AccessLevel.Manager},
                new User {Id = 1, UserName = "Chris", Password = "111", AccessLevel = AccessLevel.Agent},
                new User {Id = 1, UserName = "Donald", Password = "222", AccessLevel = AccessLevel.ExternalUser}
            };

            _user = new UserDetail(1,"Raj");
            _appConfig = Substitute.For<IApplicationConfiguration>();
            _userRepository = Substitute.For<IUserRepository>();
        }


        [Fact]
        public void GetAllUsers_ReturnsNonEmptyList()
        { 
            var allUsers = _userRepository.GetAllUsers().Returns(_listOfUsers);
            Assert.NotNull(allUsers);
        }

        [Fact]
        public void GivenAUserId_WhenCallingUserDetails_ThenShouldReturnNonEmptyObject()
        {
            int userId = 1;
            var userDetails = _userRepository.GetUserDetails(userId).Returns(_user);
            Assert.NotNull(userDetails);
        }

        [Fact]
        public void GivenUserDetails_WhenUpdatingUser_ShouldUpdateUser()
        {
            var updatedUser = new UserDetail(1,"Ediri");
            var userRepository = new UserRepository(_appConfig);
            var result = _userRepository.UpdateUser(updatedUser).Returns(true);
           // Assert.True(result);
        }

    }
}