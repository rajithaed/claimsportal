﻿using System;
using System.Collections.Generic;
using ClaimsPortal.API.AppConfig;
using ClaimsPortal.API.Models;
using Microsoft.Dnx.Compilation;

namespace ClaimsPortal.API.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IApplicationConfiguration _appConfig;
        private readonly Users _users;

        public UserRepository(IApplicationConfiguration appConfig)
        {
            _appConfig = appConfig;
            _users = new Users(_appConfig);
        }

        public IEnumerable<User> GetAllUsers()
        {
            return _users.GetAllUsersFromDb();
        }

        public UserDetail GetUserDetails(int userId)
        {
            return _users.GetUserDetailsFromDb(userId);
        }

        public bool UpdateUser(UserDetail updatedUser)
        {
            return _users.UpdateUserDetails(updatedUser);
        }
    }
}