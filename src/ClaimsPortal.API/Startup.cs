﻿using System;
using System.IO;
using System.Linq;
using ClaimsPortal.API.AppConfig;
using ClaimsPortal.API.Helpers;
using ClaimsPortal.API.Logging;
using ClaimsPortal.API.Repositories;
using log4net.Config;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Dnx.Runtime;
using Microsoft.Framework.Configuration;
using Microsoft.Framework.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ClaimsPortal.API
{
    public class Startup
    {
        private readonly IApplicationEnvironment _appEnvironment;
        private readonly IConfiguration _configuration;

        public Startup(IHostingEnvironment env, IApplicationEnvironment appEnvironment)
        {
            AppDomain.CurrentDomain.UnhandledException += GlobalLogging.LogUnHandledException;

            _appEnvironment = appEnvironment;
            var configurationBuilder = new ConfigurationBuilder(_appEnvironment.ApplicationBasePath)
                .AddJsonFile("config.json")
                .AddEnvironmentVariables();

            _configuration = configurationBuilder.Build();
        }

        // This method gets called by a runtime.
        // Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            SetupIoC(services);
            var setJsonSettings = (Action<JsonSerializerSettings>)
                (settings => { settings.Converters.Add(new StringEnumConverter {CamelCaseText = false}); });

            services.AddMvc(options =>
                            {
                                XmlConfigurator.Configure(
                                    new FileInfo(_appEnvironment.ApplicationBasePath + "/log4net.Config")
                                    );

                                var position = options.OutputFormatters.ToList()
                                    .FindIndex(f => f is JsonOutputFormatter);

                                var settings = new JsonSerializerSettings();
                                setJsonSettings(settings);
                                var formatter = new JsonOutputFormatter {SerializerSettings = settings};

                                options.OutputFormatters.Insert(position, formatter);
                            });

            services.Configure((Action<MvcJsonOptions>)(options => setJsonSettings(options.SerializerSettings)));

            // Uncomment the following line to add Web API services which makes it easier to port Web API 2 controllers.
            // You will also need to add the Microsoft.AspNet.Mvc.WebApiCompatShim package to the 'dependencies' section of project.json.
            // services.AddWebApiConventions();
        }

        private void SetupIoC(IServiceCollection services)
        {
            var applicationConfiguration = new ApplicationConfiguration(_configuration);
            services.AddInstance<IApplicationConfiguration>(applicationConfiguration);
            services.AddSingleton<IUserRepository, UserRepository>();
        }

        // Configure is called after ConfigureServices is called.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // Configure the HTTP request pipeline.
            app.UseStaticFiles();

            // Add MVC to the request pipeline.
            app.UseMvc();
            // Add the following route for porting Web API 2 controllers.
            // routes.MapWebApiRoute("DefaultApi", "api/{controller}/{id?}");

            var httpContextAccessor = app.ApplicationServices.GetRequiredService<IHttpContextAccessor>();
            HttpContextExtensionsHelper.Configure(httpContextAccessor);
        }
    }
}