using Microsoft.AspNet.Http;

namespace ClaimsPortal.API.Helpers
{
    public static class HttpContextExtensionsHelper
    {
        private static IHttpContextAccessor HttpContextAccessor;

        public static void Configure(IHttpContextAccessor httpContextAccessor)
        {
            HttpContextAccessor = httpContextAccessor;
        }

        public static HttpContext GetHttpContext()
        {
            return HttpContextAccessor.HttpContext;
        }
    }
}